var currentSlideIndex,
    total,
    is_video_playing = false,
    players = [],
    sliderIndex;
var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');

function startYoutube() {
  // set iframe API
  var player;
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  function onYouTubeIframeAPIReady() { // youtube API
    player = new YT.Player('player', {
        videoId: 'l-gQLqv9f4o',
        events: {
          'onReady': onPlayerAllReady,
          'onStateChange': onPlayerStateChange
        }
    });
  }

  window.onYouTubeIframeAPIReady = function () {
    $('.youtube-video').each(function (idx) {
      //div id 重複
      var video_id = $(this).attr('id');
      var div_id = video_id + Math.floor(Math.random() * 1000);
      $(this).attr('id', div_id);
      players[idx] = new YT.Player(div_id, {
          videoId: video_id,
          events: {
              'onReady': onPlayerAllReady,
              'onStateChange': onPlayerStateChange(div_id)
          }
      });
    });
    // console.log("players",players);
    // console.log("players_banner:",players_banner);
  };

  function onPlayerAllReady() {

  }

  function onPlayerStateChange(player_id){ // youtube API State change
    return function(event) {
      var stats = event.target.getPlayerState();
    //   console.log(stats);
      switch (stats) {
        case -1: //unstarted
          is_video_playing = false;
          // detactIndexSlide();
          break;
        case 0: //end
          is_video_playing = false;
          // detactIndexSlide();
          break;
        case 1: //playing
          is_video_playing = true;
          // detactIndexSlide();
          // console.log(is_video_playing);
          // stop_other(event.target);
          break;
        case 2: //paused
          is_video_playing = false;
          // detactIndexSlide();
          break;
        case 3: //buffering
          is_video_playing = true;
          // detactIndexSlide();
          break;
      }
    };
  }
}

(function(){


})()

function bindDeviceNav() {
  var anchor = $(this).data('menuanchor');
  var winWidth = $(window).outerWidth();
  var scrolltop ;
  if(winWidth < 480){
    scrolltop = 60;
  } else{
    scrolltop = 80;
  }
  switch (anchor) {
    case "Home":
      $body.animate({scrollTop: 0}, 700, 'easeInOutCirc');
      // console.log(anchor);
      break;
    case "Products":
      $body.animate({scrollTop: $('section.products').offset().top - scrolltop}, 700, 'easeInOutCirc');
      // console.log(anchor);
      break;
    case "Video":
      $body.animate({scrollTop: $('section.film').offset().top - scrolltop}, 700, 'easeInOutCirc');
      // console.log(anchor);
      break;
    case "Event":
      $body.animate({scrollTop: $('section.event').offset().top - scrolltop}, 700, 'easeInOutCirc');
      // console.log(anchor);
      break;
  }
  return false;
}
var gobackTimeout;
function detactIndexSlide() { // detact index slider
  total = sliderIndex.getSlideCount();
  // console.log("total",total);
  sec = 0;
  if( currentSlideIndex < total ){ // not the end
    resetIndexSlide();
  }
  if( currentSlideIndex === total){
    sliderIndex.stopAuto();
    gobackTimeout = setTimeout(function(){
      sliderIndex.goToSlide(0);
      sec = 0;
      currentSlideIndex = 1;
    //   console.log("currentSlideIndex",currentSlideIndex);
    //   console.log("total",total);
      sliderIndex.startAuto();
    },7000);
  }
}

function resetIndexSlide() {
  sliderIndex.stopAuto();
  setTimeout(function(){
    sliderIndex.startAuto();
  }, 100)
}

// function stop_other(target){
//   if( players.length > 0){
//     for (i = 0; i < players.length; i++) {
//         if (target != players[i]) {
//             if (players[i].getPlayerState() != -1) {
//                 players[i].stopVideo();
//             }
//         }
//     }
//   }
// }


var sec = 0;
function countDownConsole() {
  setInterval(function(){
    sec ++;
    // console.log("sec:",sec);
  },1000);
}

function stop_all(){
  for (i = 0; i < players.length; i++) {
      players[i].stopVideo();
  }
}
var currentSlideIndex;
sliderIndex = $('.bxSlide-index').bxSlider({
  pager: false,
  auto: false,
  pause: 7000,
  infiniteLoop: false,
  hideControlOnEnd: true,
  touchEnabled: false,
  responsive: false,
  onSliderLoad: function(){
    currentSlideIndex = 1;
    countDownConsole();
  },
  onSlidePrev: function(currentSlide) {
    currentSlideIndex = currentSlideIndex - 1;
    // console.log("currentSlideIndex",currentSlideIndex);
    detactIndexSlide();
    clearTimeout(gobackTimeout);
  },
  onSlideNext: function(currentSlide) {
    currentSlideIndex = currentSlideIndex + 1;
    // console.log("currentSlideIndex",currentSlideIndex);
    detactIndexSlide();
  }
});

var sliderProductImage = $('.bxSlide-product-image').bxSlider({
  // mode: 'fade',
  pager: false,
  touchEnabled: false,
  // adaptiveHeight: true,
  onSliderLoad: function(){
    setProductImageHeight();
  }
});

var sliderProduct = $('.bxSlide-product').bxSlider({
  mode: 'fade',
  pager: true,
  infiniteLoop: false,
  hideControlOnEnd: true,
  touchEnabled: false,
  onSliderLoad: function(currentSlide){
    $('.product-slide').find('.bx-pager-item').eq(0).addClass('is-active');
    setProductThumb();
  },
  onSlideBefore: function(currentSlide, totalSlides, currentSlideHtmlObject){
    gotoSlide(currentSlideHtmlObject);
    $('.product-slide').find('.bx-pager-item').removeClass('is-active');
    $('.product-slide').find('.bx-pager-item').eq(currentSlideHtmlObject).addClass('is-active');
  }
});

var sliderVideo = $('.bxSlide-video').bxSlider({
  responsive: true,
  touchEnabled: false,
  infiniteLoop: false,
  onSliderLoad: function(){
    var videolist = $('.bxSlide-video').find('li');
    var i = 0;
    var vid = [];
    videolist.each(function(){
      var attrSrc = $(this).find('iframe').attr('src');
      if (typeof attrSrc !== typeof undefined && attrSrc !== false) {
        vid[i] = $(this).find('iframe').attr('src').split('?')[0].split('embed/')[1];
      } else {
        vid[i] = $(this).find('iframe').attr('id');
      }
      $('.video-menu').find('li').eq(i).css('background-image', 'url(http://img.youtube.com/vi/' + vid[i] + '/mqdefault.jpg)');
      i = i + 1;
    });
    $('.video-menu').find('li').eq(0).addClass('is-active');
    $('.video-list').find('.bx-pager-item').eq(0).addClass('is-active');
    $('.video-menu').find('li').on('click', function(){
      var eq = $(this).index();
      sliderVideo.goToSlide(eq);
      $('.video-menu').find('li').removeClass('is-active');
      $(this).addClass('is-active');
      return false;
    })
  },
  onSlideBefore: function(currentSlide, totalSlides, currentSlideHtmlObject){
    $('.video-list').find('.bx-pager-item').removeClass('is-active');
    $('.video-list').find('.bx-pager-item').eq(currentSlideHtmlObject).addClass('is-active');
  },
  onSlideAfter: function(){
    stop_all();
  }
});

var eventlist = $('.bxSlide-event').find('li');
if (eventlist.length > 0){
  var eventCount = eventlist.length;
  var sliderEvent = $('.bxSlide-event').bxSlider({
    pager: false,
    infiniteLoop: false,
    hideControlOnEnd: true,
    touchEnabled: false,
    startSlide: eventCount - 1,
    onSliderLoad: function ($slideElement, oldIndex, newIndex){
      createTimeline(eventCount);
      setTimelineUL();
    },
    onSlidePrev: function($slideElement, oldIndex, newIndex){
    // console.log('prev');
    var slide_count = sliderEvent.getSlideCount();
    timelineMove(newIndex,eventCount);
    },
    onSlideNext: function($slideElement, oldIndex, newIndex){
    timelineMove(newIndex,eventCount);
    }
  });
} else {
  $('.event-list').html('<p class="msg">目前沒有任何活動</p>');
  $('.event-timeline').hide();
}

function setProductThumb() { // set product thumb
  var productList = $('.product-slide').find('li');
  var i = 0;
  var thumbImg = [];
  var pager = $('.product-slide').find('.bx-pager');
  productList.each(function(){
    var imgSrc = $(this).find('img').attr('src');
    pager.find('.bx-pager-item').eq(i).find('a').css('background-image', 'url(' + imgSrc +')');
    i = i + 1;
  })
}

function eventSlideLoad() {
  count = sliderEvent.getSlideCount();
  createTimeline(count);
  setTimelineUL();
}

function setFullscreen() { // set full screen
  imgWidth = 1920;
  imgHeight = 1080;
  var ratio_h = imgHeight/imgWidth;
  var ratio_w = imgWidth/imgHeight;
  var w = $(window).outerWidth();
  var h = $(window).outerHeight();
  var fullimg = $('.fullscreen-img').find('img');
  var fulllist = $('.fullscreen-img').find('li');
  if ((h/w)<ratio_h){
    fullimg.width(w);
    fullimg.height(ratio_h*w);
    var nw = w;
    var nh = ratio_h*w;
  }else{
    fullimg.height(h);
    fullimg.width(ratio_w*h);
    var nw = ratio_w * h;
    var nh = h;
  }
  var margintop = Math.abs(nh/2);
  var marginleft = Math.abs(nw/2);
  fulllist.css({height: h+"px"});
  fullimg.css({marginTop: "-"+margintop+"px", marginLeft: "-"+marginleft+"px"});
  // set index slider height
  $('.fullscreen-img').find('.bx-viewport').css({
    "height": h+"px"
  });
  // setIframe();
}

function setIframe() { // set index iFrame
  var videoWidth = 630;
  var videoHeight = 472;
  $('.embed-responsive').find('iframe').each(function(){
    $(this).css({
      "width": videoWidth,
      "height": videoHeight
    })
  })
}

function destroyFullscreen(sHeight) {
  var fullimg = $('.fullscreen-img').find('img');
  var fulllist = $('.fullscreen-img').find('li');
  var fullVideo = $('.fullscreen-img').find('iframe');
  fulllist.css({"height": "auto","width": "100%"});
  fullimg.css({margin: "0","height": "","width": ""});
  $('.fullscreen-img').find('.bx-viewport').css({"height": "auto"});
  // $('.fullscreen-img').find('iframe').css({"width": "100%"});
  // $('.fullscreen-img').find('iframe').css({"height": ""});
}

function setProductImageHeight() { // set product image height
  if(!isMobile()){
    var docuHeight = $(window).outerHeight();
    $('.product-image').find('.bx-viewport').height(docuHeight);
    $('.product-image').find('li').height(docuHeight);
  }
}

function gotoSlide(i) { // slider go to
  sliderProductImage.goToSlide(i);
};
function createTimeline(slide_count) { // creat timeline
  var list = "<div class='city'></div>";
  // var list = "";
  var listCover = "";
  var listDate = "";
  var timeline = $('#timeline-list');
  for ( i = 1; i <= slide_count; i++) {
    listCover = $('.bxSlide-event li:nth-child('+i+')').find('img').attr('src');
    listDate =$('.bxSlide-event li:nth-child('+i+')').find('.date').text();
    list += "<div class='circle' data-index='"+i+"'><a href='#' style='background-image:url("+listCover+")'><span>"+listDate+"</span></a></div>"
  }
  timeline.html(list);
  timeline.find('.circle').eq(slide_count - 1).addClass('is-active');
  timeLineEvent();
}
function timeLineEvent() { // timeline event
  var list = $('#timeline-list').find('.circle');
  var total = list.length;
  list.on('click', function(){
    var index = $(this).data('index') - 1;
    timelineMove(index,total);
    sliderEvent.goToSlide(index);
    return false;
  })
}
function timelineMove(index,total){ // timeline move
  var city = $('.city');
  var list = $('#timeline-list').find('.circle');
  var listLiWidth = list.outerWidth();
  var listLiMargin = 40;
  list.removeClass('is-active');
  list.eq(index).addClass('is-active');
  index = index + 1;
  // console.log(index+","+total);
  var listMoveX = Math.abs( (index - total) * (listLiWidth + listLiMargin) ) ;
  // console.log(listMoveX);
  city.css({
    '-webkit-transform': 'translate3d('+listMoveX+'px, 0, 0)',
    'transform': 'translate3d('+listMoveX+'px, 0, 0)'
  })
  list.css({
    '-webkit-transform': 'translate3d('+listMoveX+'px, 0, 0)',
    'transform': 'translate3d('+listMoveX+'px, 0, 0)'
  });
}

function setTimelineUL() { // set timeline UL
  var cityWdith = 735;
  var listLi = $('#timeline-list').find('.circle');
  var listLiWidth = listLi.outerWidth();
  var listLiMargin = 40;
  var timeLineDivWidth = $('.event-timeline').outerWidth();
  var listUlWidth = $('#timeline-list').outerWidth();
  var listUlWidthAll = listLi.length * (listLiWidth + listLiMargin);
  // var translateX = (listUlWidthAll - timeLineDivWidth / 2) - ( listLiWidth + listLiMargin ) / 2 ;
  var translateX = listUlWidthAll - timeLineDivWidth + cityWdith;
  var listUltranslateX = listUlWidth / 2 - listLiWidth / 2;
  // console.log(listUlWidthAll+","+timeLineDivWidth+","+translateX);
  $('#timeline-list').css({
    'width': listUlWidthAll + cityWdith,
    '-webkit-transform': 'translate3d(-'+translateX+'px, 0, 0)',
    'transform': 'translate3d(-'+translateX+'px, 0, 0)'
  });
}

function mobileNav() { // set mobile nav
  $('.header').find('.logo').on('click', 'a', function(){
    if( $(window).outerWidth() <= 480 ){
      $('.nav').delay(1000).removeClass('open');
      $('.toggle').find('a').removeClass('active');
    }
  })
  $('#mainNav').find('li').on('click', 'a', function(){
    if( $(window).outerWidth() <= 480 ){
      $('.nav').delay(1000).removeClass('open');
      $('.toggle').find('a').removeClass('active');
    }
  })
}

var mobileFirst = false;
var fullpageLoaded = false;


function firstLoad() {
  var winWidth = $(window).outerWidth();
  setTimeout(function() {

  }, 2000);
  if( winWidth > 768 ){
    setFullscreen();
    creatFullPage();
    fullpageLoaded = true;
  } else {
    mobileFirst = true;
    startYoutube();
    $('#mainNav').find('li').bind('click', bindDeviceNav);
    // set index slider iframe size
    // var idsImgHeight = $('.fullscreen-img').find('img').css("height");
    // $('.fullscreen-img').find('iframe').each(function(){
    //   console.log('ok');
    // })
    // $('.fullscreen-img').find('iframe').css({"width": "100%"});
    // $('.fullscreen-img').find('iframe').css({"height": idsImgHeight});
  }
  // setTimeout(function(){
  //   startIndexSlider();
  // },3000)
  // creat index slider video background
  var slideList = $('.bxSlide-index').find('.embed-responsive');
  var i = 0;
  var vid = [];
  slideList.each(function(){
    var attrSrc = $(this).find('.youtube-video').attr('src');
    if (typeof attrSrc !== typeof undefined && attrSrc !== false) {
      vid[i] = $(this).find('iframe').attr('src').split('?')[0].split('embed/')[1];
    } else {
      vid[i] = $(this).find('.youtube-video').attr('id');
    }
    $(this).after('<div class="videoBg" style="background-image:url(http://img.youtube.com/vi/'+ vid[i] +'/mqdefault.jpg)"></div>');
    i = i + 1;
  });
}

function setProductSliderMiddle() {
    var winWidth = $(window).outerWidth();
    if ( winWidth > 768 ){
        var p_block = $('.product-block');
        var p_block_height = $('.product-block').outerHeight();
        var m_top = -(p_block_height / 2) ;
        console.log(m_top);
        p_block.css({
            "margin-top": m_top
        });
    }
}

function detectReso() {
  var winWidth = $(window).outerWidth();
  // var sectionHeight = $('.fullscreen-img').find("img").css('height');
  setTimelineUL();
  setProductSliderMiddle();
  setProductThumb();
  if( mobileFirst ){
    if( winWidth > 768 ){
      if(!fullpageLoaded){
        creatFullPage();
        mobileFirst = false;
        fullpageLoaded = true;
      }
      sliderIndex.reloadSlider();
      setFullscreen();
      $('#mainNav').find('li').unbind('click', bindDeviceNav);
    } else {
      destroyFullscreen();
      sliderIndex.reloadSlider();
    }
  } else {
    if( winWidth < 769 ){
      if(fullpageLoaded){
        $.fn.fullpage.destroy('all');
        fullpageLoaded = false;
      }
      destroyFullscreen();

      sliderIndex.reloadSlider();
      $('#mainNav').find('li').bind('click', bindDeviceNav);
    } else {
      if(!fullpageLoaded){
        creatFullPage();
        fullpageLoaded = true;
      }
      sliderIndex.reloadSlider();
      setFullscreen();
      $('#mainNav').find('li').unbind('click', bindDeviceNav);
    }
  }

}

function creatFullPage() {
  $('.page').fullpage({
   anchors: ['Home', 'Products', 'Video', 'Event'],
   menu: '#mainNav',
   sectionSelector: 'section',
   scrollOverflow: true,
   responsiveWidth: 1025,
   loopHorizontal: false,
   onLeave: function(index, nextIndex, direction){
     stop_all();
   },
   afterRender: function(){
      startYoutube();
   }
  });
}

function eachAnchor() { // stop anchor
  $('a').each(function(){
    var url = $(this).attr('href');
    if( url === "#" || url === ""){
      $(this).css({
        'cursor': "default"
      })
      $(this).bind('click',function(e){
        e.preventDefault();
      })
    }
  })
};

function isMobile() { // detect mobile
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

$(function(){
  // preload
  $('body').jpreLoader({
    // autoClose: false,
  },function(){
    $('body').addClass('is-loaded');
    $('body').removeClass('is-loading');
    setTimeout(function() {
      sliderIndex.startAuto();
    }, 100);
  });
  $('.main-nav').find('.toggle').on('click', 'a', function(e){
    e.preventDefault();
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('.nav').removeClass('open');
    } else {
      $(this).addClass('active');
      $('.nav').addClass('open');
    }
  });
  firstLoad(); // detect resolution
  eachAnchor();
  mobileNav();
  setProductSliderMiddle();
  $(window).on('resize', detectReso);
});
